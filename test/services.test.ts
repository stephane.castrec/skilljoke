'use strict';

import 'mocha';
import { assert } from 'chai';
import { getJoke } from '../src/commons/services';

describe('Service tests', () => {
    it('getJoke', async () => {
        const j = await getJoke();
        assert.isNotNull(j);
        assert.isNotNull(j.blagues);
    });
});