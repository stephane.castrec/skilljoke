
'use strict';

import { HandlerInput } from 'ask-sdk';
import { Response } from 'ask-sdk-model';

import { IHandler } from '../commons/utils/IHandler';
import { Constants } from '../commons/Constants';
import { playJoke } from '../commons/Business';

export const IntentHandler: IHandler = {
    // launch request and play intent have the same handler
    'LaunchRequest': async function (input: HandlerInput): Promise<Response> {
        await playJoke(input, true);
        return input.responseBuilder.getResponse();

    },
    'AMAZON.YesIntent': async function (input: HandlerInput): Promise<Response> {
        await playJoke(input, false);
        return input.responseBuilder.getResponse();

    },
    'AMAZON.NoIntent': async function (input: HandlerInput): Promise<Response> {
        return input.responseBuilder
            .speak(Constants.messages.bye())
            .withShouldEndSession(true)
            .getResponse();
    },
    'AMAZON.CancelIntent': async function (input: HandlerInput): Promise<Response> {
        return input.responseBuilder
            .speak(Constants.messages.bye())
            .withShouldEndSession(true)
            .getResponse();
    },
    'AMAZON.StopIntent': async function (input: HandlerInput): Promise<Response> {
        return input.responseBuilder
            .speak(Constants.messages.bye())
            .withShouldEndSession(true)
            .getResponse();
    },
    'AMAZON.HelpIntent': async function (input: HandlerInput): Promise<Response> {
        return input.responseBuilder
            .speak(Constants.messages.help())
            .reprompt(Constants.messages.reprompt())
            .withShouldEndSession(false)
            .getResponse();
    },
    'SessionEndedRequest': async function (input: HandlerInput): Promise<Response> {
        // No session ended logic
        // do not return a response, as per https://developer.amazon.com/docs/custom-skills/handle-requests-sent-by-alexa.html#sessionendedrequest
        return Promise.resolve(input.responseBuilder.getResponse());
    },
    'System.ExceptionEncountered': async function (input: HandlerInput): Promise<Response> {
        console.log("\n******************* EXCEPTION **********************");
        console.log("\n" + JSON.stringify(input.requestEnvelope, null, 2));
        return input.responseBuilder
            .speak(Constants.messages.error())
            .withShouldEndSession(false)
            .getResponse();
    },
    'Unhandled': async function (input: HandlerInput): Promise<Response> {
        return this['System.ExceptionEncountered'];
    }
}