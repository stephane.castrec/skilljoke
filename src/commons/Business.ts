import { Constants } from './Constants';
import { getJoke } from './services.js';
import { HandlerInput } from 'ask-sdk';

const Entities = require('html-entities').AllHtmlEntities;

export const playJoke = async (input: HandlerInput, isWelcome: boolean) => {
    let msg;
    if (isWelcome) {
        msg = Constants.messages.welcome();
    } else {
        msg = Constants.messages.next();
    }
    try {
        const joke = await getJoke();
        const j = joke.blagues.replace(/\./g, ". ");
        const question = Constants.messages.reprompt();
        const entities = new Entities();
        const f = entities.decode(j);
        input.responseBuilder.speak(`${msg} <break time="0.5s"/> ${f} <break time="0.8s"/> ${question}`)
            .reprompt(Constants.messages.reprompt())
    } catch (e) {
        input.responseBuilder.speak(Constants.messages.error())
            .withShouldEndSession(true);
    }
    input.responseBuilder.reprompt(Constants.messages.reprompt())
}   
