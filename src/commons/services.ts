import * as request from 'request-promise-native';
import { getOne } from './utils/utils';

const url = "https://bridge.buddyweb.fr/api/blagues/blagues";
/*
[{"id":"271","fact":"http:\/\/www.chucknorrisfacts.fr\/img\/upload\/q48da669cd83d2.jpg","date":"1222288034","vote":"5494","points":"21503"}]
*/
export interface Joke {
    blagues: string;
}

export const getJoke = async (): Promise<Joke> => {
    var options = {
        uri:url,
        json: true
    };

    const jokes = await request.get(options);
    return getOne(jokes);
}