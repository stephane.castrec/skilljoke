import { ui } from 'ask-sdk-model';
import { getOne } from './utils/utils';


export const Constants = {
    messages: {
        welcome: () => {
            return 'bienvenue sur Mes blagues.'; 
        },
        next: () => {
            return 'En voilà une autre.';
        },
        error: () => {
            return 'Il semblerait que mes blagues est soit cassé. Ré-essais dans un instant.';
        },
        help: (): string => {
            return "Avec mes blagues, accédez aléatoirement à un grand nombre de blagues. Je vous en dis une ?";
        },
        not_supported: () => {
            return "Cette skill ne supporte pas cet appareil";
        },
        reprompt: () => {
            return "Souhaitez vous une autre blague ?"
        },
        bye: () => {
            return "Mes blagues dit à bientôt";
        }
    }
}